/*
  Simple sketch for Roomba 600 VirtualWall
  Created by Drew Gibson, December 31, 2017.
  Released into the public domain.
*  
 *  Pins Used:
 *  
 *  PCB  Physical Port pin        Use for
 *  RST                           Reset         Ground momentarily to reset uController
 *  ??   ??       ?               Indicator (flash for low battery?)
 *  ??   ??       ?               Reserved for USB serial
 *  ??   ??       ?               VW_PIN        VirtualWall IR LED
 *  ??   ??       ?               FF_PIN        ForceField IR LED
 *  ??   ??       ?               MODE_BTN      Mode pin on Power/Mode switch
 *  
*/

#include <boarddefs.h>
#include <ir_Lego_PF_BitStreamEncoder.h>
#include <IRremote.h>
#include <IRremoteInt.h>
#include <LowPower.h>

// Instantiate IRremote on Pin 3.
IRsend irsend;

// Global vars
byte roomba_cmnd = 162;
bool buttonState = false;

void setup() {
  // Init IR with 38kHz carrier
  irsend.enableIROut(38);
  // Read mode pin for VW or FF
  // pinMode(MODE_BTN, INPUT_PULLUP);
  // MODE_BTN = 
  // Set roomba_cmnd t VW, 162 or FF, 161
  // buttonState = digitalRead(MODE_BTN)
  if ( buttonState ) {
    roomba_cmnd = 161;
  }
}

void roomba_send_cmnd(byte cmnd_code) {
  for (int i=0; i <= 1; i++) {
    for (int b=7; b >= 0; b--) {
      if bitRead( cmnd_code, b) {
        irsend.mark(3000);
        irsend.space(1000);
      }
      else {
        irsend.mark(1000);
        irsend.space(3000);
      }
    }
     if ( i == 0 ) {
      irsend.space(1000);
      // LowPower.powerDown(SLEEP_15MS, ADC_OFF, BOD_OFF);
    }
  }
}

void loop() {
  roomba_send_cmnd(roomba_cmnd);
  // LowPower.powerDown(SLEEP_250MS, ADC_OFF, BOD_OFF);
  LowPower.powerDown(SLEEP_500MS, ADC_OFF, BOD_OFF);
}

