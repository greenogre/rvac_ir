/*
  Simple sketch for Roomba 600 VirtualWall
  Created by Drew Gibson, December 31, 2017.
  Released into the public domain.
*/

#include <boarddefs.h>
#include <ir_Lego_PF_BitStreamEncoder.h>
#include <IRremote.h>
#include <IRremoteInt.h>

// Instantiate IRremote on Pin 3.
IRsend irsend;

void setup() {
  irsend.enableIROut(38);
  // Read switch for mode [VirtualWall|ForceField|Remote] here
  
}

void roomba_send_cmnd(byte cmnd_code) {
  for (int i=0; i <= 1; i++) {
    for (int b=7; b >= 0; b--) {
      if bitRead( cmnd_code, b) {
        irsend.mark(3000);
        irsend.space(1000);
      }
      else {
        irsend.mark(1000);
        irsend.space(3000);
      }
    }
    irsend.space(16000);
  }
}

void loop() {
  // ForceField
  // roomba_send_cmnd(161)
  // VirtualWall
  roomba_send_cmnd(162);
}

