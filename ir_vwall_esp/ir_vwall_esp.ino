#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

extern "C" {
  #include "user_interface.h"
}

#include <dummy.h>

/*  Sketch to create VirtualWall and ForceField barriers  
 *  for robot vacuums.
 *  Compatible with Roomba 500, 600, 700, 800 and 900 Series. 
 *  
 *  This sketch is specifically for the Adafruit Feather HUZZAH ESP8266
 *  
 *  Pins Used:
 *  
 *  PCB  Physical Port pin        Use for
 *  RST                           Reset         Ground momentarily to reset ESP8266
 *  EN                            Soft power switch (disables 3.3v regulator, low=power off)
 *  0    12       IO0 (Red LED)   Indicator (flash for low battery?)
 *  TX   16       IO1             Reserved for USB serial
 *  2    11       IO2 (Blue LED)  Indicator (use for wifi connect indicator?)
 *  RX   15       IO3             Reserved for USB serial
 *  4    13       IO4             VW_PIN        VirtualWall IR LED
 *  5    14       IO5             FF_PIN        ForceField IR LED
 *                IO6-11          Reserved
 *  12    6       IO12            MODE_BTN      Mode pin on Power/Mode switch
 *  13    7       IO13            CLEAN_BTN     Clean button switch
 *  14    5       IO14            DOCK_BTN      Dock button switch
 *  15   10       IO15            RC_PIN        Remote Control IR LED
 *  16    4       IO16            Wake from deep sleep.
 *  
 */

# define VW_PIN 4                   // VirtualWall IR LED     (output)
# define FF_PIN 5                   // ForceField IR LED      (output)
# define RC_PIN 15                  // Remote Control IR LED  (output)
# define PWR_PIN 0                  // Power indicator        (built-in red LED) 
# define NET_PIN 2                  // Network indicator      (built-in Blue LED)
# define MODE_BTN 12                // VW/FF select           (input, active LOW)
# define CLEAN_BTN 13               // CLEAN Button           (input, active LOW)
# define DOCK_BTN 14                // DOCK Button            (input, active LOW)

# define IR_FREQUENCY 38000         // Infra-red carrier frequency in Hz (38 kHz)
# define DEBOUNCE_DELAY 48          // Length of debounce delay when reading buttons

const char* ssid = "Sto Lat";
const char* password = "xxxxxxxxxxxx";

unsigned long nextCheck = 0;        // Initialise button check timer
const unsigned long BTN_CHK = 224;  // Minimum delay between reading button state (in mS)

byte buttonState = 255;             // bit 0 = device mode (1=ForceField, 0=VirtualWall)
                                    // bit 1 = Clean ( 0 = Clean command pending )
                                    // bit 2 = Dock  ( 0 = Dock command pending )
                                    // bit 7 = Disable IR output ( 0 = disabled )

void setup() {
  // Configure and turn on power indicator
  pinMode(PWR_PIN, OUTPUT);
  digitalWrite(PWR_PIN, LOW);
  // Turn off network indicator
  pinMode(NET_PIN, OUTPUT);
  digitalWrite(NET_PIN, HIGH);
  // Set PWM frequency to IR carrier frequency
  analogWriteFreq(IR_FREQUENCY);
  // Ensure IR LEDS off
  //analogWrite(VW_PIN, 0);
  //analogWrite(FF_PIN, 0);
  //analogWrite(RC_PIN, 0);
  // Setup pin for config switch and buttons(ACTIVE_LOW)
  pinMode(MODE_BTN, INPUT_PULLUP);
  pinMode(CLEAN_BTN, INPUT_PULLUP);
  pinMode(DOCK_BTN, INPUT_PULLUP);

  // Wait for switch to settle then read device mode. VirtualWall (LOW) or ForceField (HIGH)
  delay(DEBOUNCE_DELAY);
  bitWrite( buttonState, 0, digitalRead(MODE_BTN) );
  Serial.begin(74880);
}

void initWifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while ((WiFi.status() != WL_CONNECTED)) {
     delay(500);
     Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected: "); Serial.println(ssid);
  Serial.print("    IP address: "); Serial.println(WiFi.localIP());
}

void write_one(int outPin) {
  analogWrite(outPin, 512);
  delayMicroseconds(3000);
  analogWrite(outPin, 0);
  delayMicroseconds(1000);
}

void write_zero(int outPin) {
  analogWrite(outPin, 512);
  delayMicroseconds(1000);
  analogWrite(outPin, 0);
  delayMicroseconds(3000);
}

// Send single command code (32mS)
void send_code(byte code, int pin) {
  for (int b=7; b >= 0; b--) {
    if bitRead( code, b) {
      write_one(pin);
    }
    else {
      write_zero(pin);
    }
  }
}

// Send full command code (80mS)
void send_cmnd(byte cmnd_code, int pin) {
  send_code(cmnd_code, pin);
  delay(16);
  send_code(cmnd_code, pin);
}

byte buttonScan() {
  byte btnState = 255;
  boolean cleanState = digitalRead(CLEAN_BTN);
  boolean dockState = digitalRead(DOCK_BTN);
  delay( DEBOUNCE_DELAY );
  boolean cleanState_2 = digitalRead(CLEAN_BTN);
  boolean dockState_2 = digitalRead(DOCK_BTN);
  if ( cleanState_2 == cleanState ) {
     bitWrite( btnState, 1, cleanState );
  }
  if ( dockState_2 == dockState ) {
     bitWrite( btnState, 2, dockState );
  }
  return ~btnState;
}

byte networkScan() {
  byte netCmnds = 255;
  // get Mode (bit 0), Clean (bit 1), Dock (bit 2), Disable (bit 7) commands from network
  // 0 = active
  // ToDo: write WiFi command input
  return ~netCmnds;
}

int checkBattery() {
  // read the battery level from the ESP8266 analog in pin.
  // analog read level is 10 bit 0-1023 (0V-1V).
  // our 1M & 220K voltage divider takes the max
  // lipo value of 4.2V and drops it to 0.758V max.
  // this means our min analog read value should be 580 (3.14V)
  // and the max analog read value should be 774 (4.2V).
  int level = analogRead(A0);

  // convert battery level to percent
  level = map(level, 580, 774, 0, 100);
  return level;
}

void loop() {
  // Read commands from WiFi into buttonState
  //buttonState = buttonState ^ networkScan();
  
  // Read command buttons and xor into buttonState
  buttonState = buttonState ^ buttonScan();

  //Serial.print(buttonState);
  //Serial.print("\n");

  if ( !bitRead( buttonState, 1) ) {          // Check Clean Button
    send_cmnd( 136, RC_PIN );
    bitSet( buttonState, 1 );  
  }
  else if ( !bitRead( buttonState, 2) ) {     // Check Dock Button
    send_cmnd( 143, RC_PIN );
    bitSet( buttonState, 2 );
  }
  
  // Next button check is at "now" + BTN_CHK mS
  nextCheck = millis() + BTN_CHK;

  // Send FF and/or VW commands until nextCheck
  while ( millis() <  nextCheck ) {
    if ( bitRead( buttonState, 0) ) {         // Send ForceField
      send_cmnd(161, FF_PIN);
    }
    else {                                    // Send VirtualWall
      send_cmnd(162, VW_PIN);
    }
    delay(16);
    // ESP.lightSleep(16000);
  }
  
}
